import logo from './logo.svg';
import './App.css';
import ReactHowler from 'react-howler';
import React from 'react';
import {BrowserRouter as Rutas,Route, Link, Switch} from 'react-router-dom';
import Button from './Components/Button';
import Reproductor from './Components/Reproductor';
import Home from './pages/Home';
import Game from './pages/Game';
import Music from './pages/Music';
import Grupo1 from './pages/Grupo1';
import Grupo2 from './pages/Grupo2';
import Grupo3 from './pages/Grupo3';
import Grupo4 from './pages/Grupo4';
import Grupo5 from './pages/Grupo5';

class App extends React.Component{
  render () {
    return (
      
      <Rutas> 
        <h1>Hola soy Alexander</h1>   
        
       <Link to="/">Inicio</Link>   
       <Link to="/game">Juego</Link>   
       <Link to="/music">Musica</Link>   
       <Link to="/grupo1">Grupo1</Link>   
       <Link to="/grupo2">Grupo2</Link>   
       <Link to="/grupo3">Grupo3</Link>   
       <Link to="/grupo4">Grupo4</Link>   
       <Link to="/grupo5">Grupo5</Link>
       <Switch>
          <Route exact path="/">
            <Home nombre="Página Inicio" logotipo="https://www.upds.edu.bo/wp-content/uploads/2020/07/logotipo-upds-azul.png"
            
            
            />
            <Home nombre="ddd okkk" logotipo="https://cdn.icon-icons.com/icons2/2407/PNG/512/gitlab_icon_146171.png"/>
          </Route>
          <Route path="/game">
            <Game/>
          </Route>
          <Route path="/grupo1">
            <Grupo1/>
          </Route>
          <Route path="/grupo2">
            <Grupo2/>
          </Route>
          <Route path="/grupo3">
            <Grupo3/>
          </Route>
          <Route path="/grupo4">
            <Grupo4/>
          </Route>
          <Route path="/grupo5">
            <Grupo5/>
          </Route>

          <Route path="/music">
            <Music list={ [{title: 'LLorando se fue', artist: 'Kjarkas'},
                          {title: 'La bomba', artist: 'Azul Azul'},
                          {title: 'Enamorarte de alguien mas', artist: 'Morat'} ]}/>
          </Route>
        </Switch>
      </Rutas>                                    
    )
  }
}

export default App;
