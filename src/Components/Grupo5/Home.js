import React, { Component } from "react";
import Carousel from "./Carousel";

export default class Home extends Component {
  render() {
    return (
      <div>
        <Carousel />
        <h1>Home</h1>
        <p>This is home.</p>
      </div>
    );
  }
}
