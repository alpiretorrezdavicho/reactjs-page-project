import Carousel from "react-bootstrap/Carousel";

function UncontrolledExample() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://wallpapercave.com/wp/wp2465923.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>React</h3>
          <p>
            biblioteca de Javascript de código abierto diseñada para crear
            interfaces de usuario con el objetivo de facilitar el desarrollo de
            aplicaciones en una sola página.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://velog.velcdn.com/images/iaj0204/post/858b3c29-421d-4893-993e-dc000ea03641/image.jpeg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>JavaScript</h3>
          <p>
            lenguaje de programación interpretado, dialecto del estándar
            ECMAScript.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.bellcodess.com/images/html5.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>HTML</h3>
          <p>
            HTML, siglas en inglés de HyperText Markup Language, hace referencia
            al lenguaje de marcado para la elaboración de páginas web.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://hello-sunil.in/wp-content/uploads/2021/02/Media-Queries-in-CSS.png"
          alt="fourth slide"
        />

        <Carousel.Caption>
          <h3>Lenguaje de programación CSS</h3>
          <p>
            CSS, en español «Hojas de estilo en cascada», es un lenguaje de
            diseño gráfico para definir y crear la presentación de un documento
            estructurado escrito en un lenguaje de marcado.​
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default UncontrolledExample;
