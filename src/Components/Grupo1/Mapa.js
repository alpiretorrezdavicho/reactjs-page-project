import React from "react";
export default class Mapa extends React.Component {
   render() {
      return <div id='map' style={{height:'80vh', width:'80vw', backgroundColor:'red'}}></div>
   }
   componentDidMount() {
      this.initMap()
   }
   initMap() {
     let map = new window.google.maps.Map(document.getElementById("map"), {
         center: { lat: -17.738267, lng: -63.087960 },
         zoom: 13,
     });
     let marker = new window.google.maps.Marker({
       position:  { lat: -17.738267, lng: -63.087960 },
       map,
       title: 'Hello World!',
     })
   } 
}