import React from "react";
import ReactHowler from 'react-howler';
import Mapa from "../Components/Grupo1/Mapa";
import TetrisGame from "../Components/TetrisGame";
import '../App.css';


class Home extends React.Component{        
    render () {
        return (
        <div>
            <h1>GRUPO 1</h1>
            <p>
                Deiby Sejas <br />
                Alexander Sanchez
            </p>
            <Mapa/> 
            <TetrisGame nombre="Alexander" /> 
       
        </div>
        );
    }
}
export default Home;