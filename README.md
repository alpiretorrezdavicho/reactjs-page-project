# Pagina simple en React JS

Proyecto desarrollado en conjunto con los estudiantes de la UPDS-Santa Cruz.
 - Alexander Sanchez Salces
 - Pedro Deiby Sejas churco
 - Jose Andres Galarza Chavez
 - Pablo Miranda Daguer
 - Alexander Viruez
 - Mauricio Alexander Flores Morales
 
## Pasos
### Paso 1
Ubicarse en una carpeta en su sistema operativo
### Paso 2. Clonar el proyecto
`git clone git@gitlab.com:tecnoprofe/reactjs-page-project.git`

### Paso 3. Entrar en la carpeta
`cd reactjs-page-project`

### Paso 4. Instalar
`npm install`

### Paso 5. Ejecutar
`npm start`
